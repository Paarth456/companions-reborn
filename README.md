<div align="center">

<table align="center"><tr><td align="center" width="9999">
<img src=https://i.imgur.com/BSqOOeP.png align="center" width="450" alt="Project icon">


Companions is a plugin that has been around for quite a while. Recently, it was decided to be recoded into Companions Reborn and to be placed onto Spigot's Marketplace so you'd choose this plugin over other newer replica "companions" plugins!


**TO PURCHASE THIS RESOURCE, YOU HAVE TO HAVE AN ACCOUNT ON SPIGOT.ORG, YOU CAN FIND THE OFFICIAL RESOURCE PURCHASE LINK HERE - https://www.spigotmc.org/resources/companions-reborn-create-your-own-custom-companions-with-abilities-eula-compliant-1-8-1-14-4.68391/**


</td></tr></table>





![Image of gif1](https://i.imgur.com/G7iFH9Z.gif) ![Image of gif2](https://i.imgur.com/2B7U954.gif)
---
<br>
<br>![Image of overView](https://i.imgur.com/YVNjqbp.png)

Companions allows you to easily create your own custom companions (endless possibilities really), for your players to own. Upon activating a companion, it follows you beside your shoulder (or where you configure it to be) and gives you abilities.

**You can easily share your Companion creations with other plugin buyers as well! This is the beauty of Companions!**

*(Each companions can have any amount of abilities, read the WIKI here to learn how to create your own custom companions.)* 



<br>

---
<br> 

![Image of features](https://i.imgur.com/yMvCpsT.png)</div>
* **The whole plugin is configurable** - you are able to create custom companions, tilt how they look, which abilities they give, what item they hold, what name they have, etc. GUI & Abilities are also customizable.
* **Supports the creation of Custom Companions** - as aforementioned, the possibilities are endless, it's up to you to create the perfect Companions for your players.
* **Implement a CSGO Skin-like system** into your server just by adding different looking Companions but with the same abilities as a collectible system. - e.g, multiple looking Dragons but all gives Fly & Resistance.
* **Adjust body parts of a Companion**, to make it unique only to your server. - This would allow you to make amazing looking Companions.
* **Custom Abilities that are like custom enchantments** (UP TO 50 ABILITIES TO CHOOSE FROM) - You're able to give PVP-like abilities when defending/attacking or a static potion effect. Also, maybe you fancy some amazing abilities like leaping ability, flying ability, or even fireball ability. There is also a COMMAND ability that allows you to execute command(s) every x minutes on x chance! (This allows for custom abilities) - More on the wiki.
* **UP TO 15 PRE-LOADED COMPANIONS** - For you to modify, get inspiration from or copy from! Custom Companion Creation can't get any easier!
* **Implemented mystery box system** for your players to obtain "mystery" Companions!
* **Trading System!** - Allow your players to trade their Companions with each other!
* **Mix & Match Animations** meaning, you can add multiple animations to an individual Companion, making the animation's outcome somewhat unique and a creation by you.
* **EASILY CONFIGURATION** - Plugin will not crash if you get a Sound/Material name wrongly! It'll just give you a friendly prompt in the console to check the name again. (supports down to Spigot 1.8)
**It's not the end!** Your players are also able to customize their own Companions by changing what their Companions can hold (weapon), their names, and also to upgrade their ability levels so their abilities are stronger!

<br>

---

<div align="center">

<br>![Image of media](https://i.imgur.com/baSMiX0.png)
![Image of media](https://i.gyazo.com/b724498a57183ebba1c4401ee4f3966d.gif)

![Image of media](https://i.gyazo.com/361170279eb44347f209aee1058e6590.png)
![Image of media](https://i.gyazo.com/ec75735917f2f4821aac3dfd482e8ea3.gif)

<br>

 

**Companion of the month goes to... THE WIZARD!**
![Image of month](https://i.gyazo.com/d209789def168ab7f174cf3c74f09d3f.png)
      
<br>

![Image of gif1](https://i.imgur.com/hwYHbaD.gif)
![Image of gif2](https://i.imgur.com/C4YMlEJ.gif)
![Image of gif1](https://i.imgur.com/btoDTpP.gif)
![Image of gif1](https://i.imgur.com/ErgzgQk.gif)

<br>



**COMPANION TOKENS**
Receive a mystery Companion upon usage!
![Image of mystery](https://i.gyazo.com/11738b7c9e42d5389a9d86b72aa5e34d.gif)

<br>



**(14/JUL/2019) - INTRODUCING PATREON REWARDS**
This feature is only LIMITED to the first five Patreons for now!

![Image of patreon](https://i.gyazo.com/a16bf1b4b77b4fc5de8e6428aee59699.gif)

<br>

You may obtain a special Companions Trail that works on EVERY server that has the Companions installed - *AFTER VERSION 1.6.1, you no longer have to wait for plugin updates. Patreon List updates every 24 hours, you should get your Patreon benefits by then.*



For more information about other Patreon tiers, please visit here -
https://www.patreon.com/astero

<br>

---

<br>

![Image of customc](https://i.imgur.com/BGMSwPL.png)

Interested in learning how to make a Custom Companion? Visit this link! - https://gitlab.com/Aster0/companions-reborn/wikis/SECTION-3/3.2-Custom-Companions
<br>

---

<br>

---

<br>

![Image of permission](https://i.imgur.com/3xbBYFd.png)
>>>
>**Player Commands:**
>(/companions)
>
>Aliases: /c
>
>`/companions` - opens up the main GUI.
>**PERMISSION NODE:** companions.player.menu
>
>`/companions shop` - opens up the Companions Shop GUI.
>**PERMISSION NODE:** companions.player.shop
>
>`/companions upgrade` - opens up the Companions Upgrade GUI.
>**PERMISSION NODE:** companions.player.upgrade
>
>`/companions owned` - opens up the Companions Owned GUI.
>**PERMISSION NODE:** companions.player.owned
>
>`/companions toggle` - toggles your active Companion back/away.
>**PERMISSION NODE:** companions.player.toggle
>
>`/companions upgrade ability` - purchase an ability level upgrade w/o entering the upgrade menu.
>**PERMISSION NODE:** companions.upgrade.ability
>
>`/companions upgrade rename` - purchase a rename upgrade w/o entering the upgrade menu.
>**PERMISSION NODE:** companions.upgrade.rename
>
>`/companions upgrade hidename` - purchase a hide name upgrade w/o entering the upgrade menu.
>**PERMISSION NODE**: companions.upgrade.hidename
>
>`/companions upgrade changeweapon` - purchase a change weapon upgrade w/o entering the upgrade menu.
>**PERMISSION NODE:** companions.upgrade.changeweapon
>
>`/companions details <player>` - checks the Companion details of the specified online player.
>**PERMISSION NODE:** companions.player.details
>
>`/companions particle` - for Patreon users to toggle their particles off or on.
>
>
><br>
>
>**Admin Commands:**
>
>Aliases: /givec, /removec, /givecitem, /ccdata
>Permission: companions.admin.< >
>
>`<> - compulsory; () - optional`
>
>`/givecompanion <player> <companion> (true)` - Gives the specified player the specified Companion. Setting true makes it so it gives it as an active Companion.
>**PERMISSION:** companions.admin.give
>
>`/removecompanion <player> <companion>` - Removes the specified Companion from the specified Player.
>**PERMISSION:** companions.admin.remove
>
>`/forceactive <player> <companion>` - forces an owned Companion onto the specified player.
>**PERMISSION:** companions.admin.forceactive
>
>`/forceupgrade <player> <upgrade>` - forces an upgrade upon specified player. (does not make player pay the upgrade fee).
>**PERMISSION:** companions.admin.forceupgrade
>
>`/givecompanionitem <player> <item> <amount>` - Gives the player the specified plugin item.
>
>**VALID ITEMS:** `companiontoken`
>**PERMISSION:** companions.admin.item
>
>`/clearcompanionsdata <player/all>` - if a player is specified, it'll only delete that player's data. If all is inputted, it'll delete EVERYONE's data. (Use with caution, you can NOT get back your data)
>**PERMISSION NODE:** companions.admin.cleardata
>
>`/companions reload` - Reloads the Companions Plugin.
>**PERMISSION:** companions.admin.reload
>
>
>
>
>`companions.buy.companionname` - allows a player to buy the specified Companion.
>>>

<br>

---

<br>

![Image of wiki](https://i.imgur.com/7PiAzJQ.png)
<br>

If you have any doubts on any features of the plugin, visit our [wiki](https://gitlab.com/Aster0/companions/wikis/home). It boasts a really comprehensive wiki, allowing you to hopefully understand each and every features of the plugin.

---

<br>


---

<br>

![Image of api](https://i.imgur.com/TGFhnbf.png)
<br>

Want to integrate Companions into your OWN plugin? We have an easy way of doing that and also a wiki documentation on our API!

Click [here](https://gitlab.com/Aster0/companions-reborn/wikis/SECTION-4/4.1-Developer's-API) to be redirected to the documentation.

---

<br>

---

<br>

![Image of placeholderapi](https://i.imgur.com/Tj2Vm1e.png)
<br>

`%companions_activecompanion%` - displays the player's active Companion.

![Image of placeholder](https://i.gyazo.com/6789dc4be72fd8707aa7a9184fc22126.png)

---

<br>

Got an issue or a suggestion? Make a new post [here](https://gitlab.com/Aster0/companions/issues), I am regularly checking them and will reply to you in less than a day max. If you ever need support on how to use a certain feature of the plugin, join the Discord support server here.


<br>

